
// variables
const cont = document.querySelector('.slots-content');

// fetching slots, name, creating play and limiting data dispay to 15
class Games {
    async getGames(){
       try{
        let results = await fetch("http://stage.whgstage.com/front-end-test/games.php")
        let data = await results.json()
        console.log(data.categories)
        let recent = data.new;
         console.log(recent)
       const modifieldData = data.map((game) => ({
                slots: game.categories.includes("slots") ? "slots" : '',
                name: game.name,
                image: game.image,
                play : !game.image.Play ? "Play" : ''

            }))
            return modifieldData.splice(0, 15);
       }catch(error){
        console.log(error)
       }
    }
}

// Displaying content
class UI {
    displayGames(slots){
        let result = '';
        //console.log(slots)
        slots.forEach(sloty => {
        //console.log(sloty);
          result +=`
          <div class="card-l box">
          <div class="ribbon"><span class="cat">${sloty.slots}</span></div>
          <div class="button"><a href="#">${sloty.play}</a></div>
            <img src=${sloty.image} class="card-img" alt="Game name" data-toggle="tooltip" data-placement="top"ti title=${sloty.name}>
          </div>
          `        
        })
        cont.innerHTML = result;
    }
}


document.addEventListener("DOMContentLoaded", () => {
    const ui = new UI()
    const games = new Games();
    // get all available games
    games.getGames().then(games => {
        ui.displayGames(games);
        console.log(games)
        
    })
})