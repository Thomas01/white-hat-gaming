
// variables
const games = document.querySelector('.card-content');

// fetching top games, name, image and limiting display to 15
class Games {
    async getGames(){
       try{
        let results = await fetch("http://stage.whgstage.com/front-end-test/games.php")
        let data = await results.json()
        console.log(data)
       const modifieldData = data.map((game) => ({
                top: game.categories.includes("top") ? "top" : '',
                name: game.name,
                image: game.image,
                 Play : !game.Play ? "Play" : ''
            }))
            return modifieldData.splice(0, 15);
       }catch(error){
        console.log(error)
       }
    }
}

// Displaying content
class UI {
    displayGames(tops){
        let result = '';
        tops.forEach(topy => {
          result +=`
          <div class="card-l box" >
          <div class="ribbon"><span class="cat">${topy.top}</span></div>
          <div class="button"><a href="#">${topy.Play}</a></div>
            <img src=${topy.image} class="card-img" alt="Game name" data-toggle="tooltip" data-placement="top" title=${topy.name}>
            </div>
          `        
        })
        games.innerHTML = result;
    }
}


document.addEventListener("DOMContentLoaded", () => {
    const ui = new UI()
    const games = new Games();
    // get all available games
    games.getGames().then(games => {
        ui.displayGames(games);
        console.log(games)
        
    })
})