
// variables
const jack = document.querySelector('.jack-content');

// fetching game, amount and limiting data display to 15
class Games {
    async getGames(){
       try{
        let results = await fetch("http://stage.whgstage.com/front-end-test/jackpots.php")
        let data = await results.json()
        console.log(data);
       const modifieldData = data.map((games) =>({
               game: games.game,
               amount: games.amount,
            }))
            return modifieldData.splice(0, 15);
       }catch(error){
        console.log(error)
       }
    }
}

// Displaying content
class UI {
    displayGames(jacks){
        let result = '';
        jacks.forEach(jacky => {
        //console.log(jacky);
          result +=`
          <div class="card mx-3 my-3" style="width: 18rem;">
          <div class="card-header">
            ${jacky.game}
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">${jacky.amount}</li>
          </ul>
        </div>
          `        
        })
        jack.innerHTML = result;
    }
}


document.addEventListener("DOMContentLoaded", () => {
    const ui = new UI()
    const games = new Games();
    // continue fetching data every second 
    let interval = setInterval(() => {
      games.getGames().then(games => {
        console.log(games)
        ui.displayGames(games)
    })
     }, 1000)


})