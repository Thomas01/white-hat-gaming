
// variables
const cont = document.querySelector('.game-content');

// fetching new games, name, image and limiting display to 15
class Games {
    async getGames(){
       try{
        let results = await fetch("http://stage.whgstage.com/front-end-test/games.php")
        let data = await results.json()
       const modifieldData = data.map((game) => ({
                new: game.categories.includes("new") ? "new" : 'slots',
                name: game.name,
                image: game.image,
                play : !game.image.Play ? "Play" : ''
            }))
            return modifieldData.splice(0, 15);
       }catch(error){
        console.log(error)
       }
    }
}

// Displaying content
class UI {
    displayGames(games){
        let result = '';
        games.forEach(game => {
          result +=`
          <div class="card-l box">
          <div class="ribbon"><span class="cat">${game.new}</span></div>
          <div class="button"><a href="">${game.play}</a></div>
            <img src=${game.image} class="card-img" alt="Game name" data-toggle="tooltip" data-placement="top" title=${game.name}>
          </div>
          `        
        })
        cont.innerHTML = result;
    }
}


document.addEventListener("DOMContentLoaded", () => {
    const ui = new UI()
    const games = new Games();
    // get all available games
    games.getGames().then(games => {
        ui.displayGames(games);
        console.log(games)
        
    })
})